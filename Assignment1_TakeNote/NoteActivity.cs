using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace Assignment1_TakeNote
{
    [Activity(Label = "AddNoteActivity")]
    public class NoteActivity : Activity
    {
        private int _NoteId;
        private Database _Database;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Get node id from the intent, if it was not passed then use the default of -1.
            _NoteId = Intent.GetIntExtra("Note", -1);

            Console.WriteLine("Note id is {0}", _NoteId.ToString());

            // Set the title that will appear at the top of the screen.
            // If an ID was passed through an intent, then set it to "Edit", otherwise "Add".
            if (_NoteId == -1)
            {
                this.Title = GetString(Resource.String.AddNoteTitle);
            }
            else
            {
                this.Title = GetString(Resource.String.EditNoteTitle);
            }

            SetContentView(Resource.Layout.Note);

            // Assign delegates to the buttons in the layout.
            AssignDelegates();

            // Create the database class instance. This is used to connect to the database.
            _Database = new Database();

            // Load note data from the database if required.
            LoadNote();
        }

        private void AssignDelegates()
        {
            // This function is just to add the delegates to the buttons in the layout.
            // Get the Cancel, Save and Delete buttons from the layout.
            Button btnCancelNote = FindViewById<Button>(Resource.Id.btnCancelNote);
            Button btnSaveNote = FindViewById<Button>(Resource.Id.btnSaveNote);
            Button btnDeleteNote = FindViewById<Button>(Resource.Id.btnDeleteNote);

            // Add a delegate that will return to the main activity when the cancel note button is pressed.
            btnCancelNote.Click += delegate {
                // Create an Intent start the main activity.
                StartActivity(new Intent(this, typeof(MainActivity)));
            };

            // Add a delegate that will save the note to the database and return to the main activity.
            btnSaveNote.Click += delegate
            {
                // Save the note.
                SaveNote();
            };

            // Add a delegate that will save the note to the database and return to the main activity.
            btnDeleteNote.Click += delegate
            {
                // Save the note.
                DeleteNote();
            };

            // If this note already exists, enable the delete button.
            if (_NoteId != -1)
            {
                btnDeleteNote.Visibility = ViewStates.Visible;
                btnDeleteNote.Enabled = true;
            }
        }

        protected void LoadNote()
        {
            // If noteId is -1, do nothing.
            if (_NoteId == -1)
                return;

            // Connect to the database.
            var db = _Database.Connect();

            // Get the note from the database.
            var note = db.Get<Notes>(_NoteId);

            // Update the text fields with the data from the database.
            FindViewById<TextView>(Resource.Id.titleText).Text = note.Title;
            FindViewById<TextView>(Resource.Id.bodyText).Text = note.Body;

            // Close the database connection.
            db.Close();
        }

        protected void SaveNote()
        {
            // Connect to the database.
            var db = _Database.Connect();

            string titleText = FindViewById<TextView>(Resource.Id.titleText).Text;
            string bodyText = FindViewById<TextView>(Resource.Id.bodyText).Text;

            if (_NoteId == -1)
            {
                // NoteId is set to -1, which means this is a new note.
                // Create a note object and populate it with the data.
                Notes note = new Notes();

                // Need to add some tests to make sure that there is some info in the title and body.
                note.Title = titleText;
                note.Body = bodyText;
                note.CreationDate = DateTime.Now;

                // Insert this note into the database.
                db.CreateTable<Notes>();
                db.Insert(note);
            }
            else
            {
                // This note already exists and we're actually updating it.
                // Get the existing note from the database.
                var note = db.Get<Notes>(_NoteId);

                // Update the title and body from the form.
                note.Title = titleText;
                note.Body = bodyText;

                db.Update(note);
            }

            // Close the database connection.
            db.Close();

            // Return to the main activity after adding the note.
            StartActivity(new Intent(this, typeof(MainActivity)));
        }

        protected void DeleteNote()
        {
            // If noteid is -1, then don't do anything.
            if(_NoteId == -1)
            {
                return;
            }

            // Create an alert dialog to warn the user that they are going to be deleting
            // this note.
            // Information on how to do this found: http://stacktips.com/tutorials/xamarin/alertdialog-and-dialogfragment-example-in-xamarin-android
            AlertDialog.Builder confirm = new AlertDialog.Builder(this);
            confirm.SetTitle("Delete Note");
            confirm.SetMessage("Are you sure you want to delete this note?");

            // Set up the delete button, if clicked the note will be removed from the database.
            confirm.SetPositiveButton("Delete", (sendAlert, args) =>
            {
                // Connect to the database.
                var db = _Database.Connect();

                db.Delete<Notes>(_NoteId);

                // Close the database connection.
                db.Close();

                // Return to the main activity after deleting the note.
                StartActivity(new Intent(this, typeof(MainActivity)));
            });

            // The cancel button will return.
            confirm.SetNegativeButton("Cancel", (senderAlert, args) =>
            {
                return;
            });

            // Create the dialog and show it.
            Dialog dialog = confirm.Create();
            dialog.Show();
        }
    }
}