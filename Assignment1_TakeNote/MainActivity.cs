﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using SQLite;
using System.IO;

namespace Assignment1_TakeNote
{
    [Activity(Label = "Assignment1_TakeNote", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        // List of items to be shown in the ListView.
        private List<Notes> _Items;

        protected override void OnCreate(Bundle bundle)
        {
            this.Title = GetString(Resource.String.ApplicationName); ;

            _Items = new List<Notes>();
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get the Add Note button from the layout.
            Button btnAddNote = FindViewById<Button>(Resource.Id.btnAddNote);

            // Add a delegate that will open the AddNode activity when the "Add Note" button is pressed.
            btnAddNote.Click += delegate {
                // Create an Intent for the Add Note Activity and start the activity.
                var noteActivity = new Intent(this, typeof(NoteActivity));
                StartActivity(noteActivity);
            };

            ShowNotes();
        }

        protected void ShowNotes()
        {
            // This function shows all the notes stored in the database.
            ListView notesView = FindViewById<ListView>(Resource.Id.viewNotes);

            // Populate the ListView with notes from the database. Using modified code from:
            // https://developer.xamarin.com/guides/android/user_interface/working_with_listviews_and_adapters/part_2_-_populating_a_listview_with_data/
            // Open the database file and connect to it.
            string dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "takenote.db3");
            var db = new SQLiteConnection(dbPath);

            try
            {
                // Insert this note into the database.
                TableQuery<Notes> notesSql = db.Table<Notes>();

                foreach (Notes note in notesSql)
                {
                    _Items.Add(note);
                }
            }
            catch(SQLiteException exception)
            {
                // An SQLite exception here most likely means that the table hasn't been created yet.
                // Write the error to the log and continue.
                Console.WriteLine(exception.Message);
            }
            finally
            {
                // Close the database connection.
                db.Close();
            }
            
            notesView.Adapter = new NoteAdapter(this, _Items);

            // Assign an event handler when a list item is clicked.
            notesView.ItemClick += OnListItemClick;
        }

        protected void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            // This function is called when a list item is clicked.
            // Create an intent and pass the id of the currently selected note to the Note activity.
            var noteActivity = new Intent(this, typeof(NoteActivity));
            noteActivity.PutExtra("Note", _Items[e.Position].NoteId);
            StartActivity(noteActivity);

        }
    }
}
