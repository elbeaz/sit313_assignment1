﻿# Take Note
## Outline
Take Note is a simple note taking app developed for Android using Xamarin.

## Activities
There are 2 main activities, Main and Notes. The Main activity displays all the notes in the database. Notes is used when adding or editing notes.

## Features
* View notes
* Edit notes
* Delete notes