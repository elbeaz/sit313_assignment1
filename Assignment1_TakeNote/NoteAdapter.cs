using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SQLite;

namespace Assignment1_TakeNote
{
    class NoteAdapter : BaseAdapter<Notes>
    {
        // This class is used to be able to populate the ListView with notes. Need this so I can have
        // Information found here: https://developer.xamarin.com/guides/android/user_interface/working_with_listviews_and_adapters/part_2_-_populating_a_listview_with_data/
        List<Notes> _Notes;
        Activity _Context;

        public NoteAdapter(Activity context, List<Notes> items) : base()
        {
            _Context = context;
            _Notes = items;
        }
        
        public override Notes this[int position]
        {
            get
            {
                // Return the Note in this position.
                return _Notes[position];
            }
        }

        public override int Count
        {
            get
            {
                // Return the number of elements in the _Notes list.
                return _Notes.Count;
            }
        }

        public override long GetItemId(int position)
        {
            // Return the NoteId for the note in this position.
            // This will be the ID used in the database, so you can lookup this particular item.
            return _Notes[position].NoteId;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            // Use the existing view.
            View view = convertView;

            if (view == null)
                // If no view exists, create a new one.
                // Using a custom list item layout I created called NoteListItem.
                view = _Context.LayoutInflater.Inflate(Resource.Layout.NoteListItem, null);

            // Add the note title and creation date to the list items.
            view.FindViewById<TextView>(Resource.Id.NoteTitle).Text = _Notes[position].Title;
            view.FindViewById<TextView>(Resource.Id.NoteDate).Text = "Created: " + _Notes[position].CreationDate.ToString("dd/MM/yyyy");

            // I want to return a truncated version of the note body as a preview. I'm using substring to do this.
            // Found information on how to achieve this here:
            // http://stackoverflow.com/questions/3566830/what-method-in-the-string-class-returns-only-the-first-n-characters

            if (_Notes[position].Body.Length <= 50)
            {
                // If the body is 50 characters or less, just return the whole thing.
                view.FindViewById<TextView>(Resource.Id.NoteBody).Text = _Notes[position].Body;
            }
            else
            {
                // Otherwise the body is too long. Truncate it and put "..." at the end to indicate there is more.
                view.FindViewById<TextView>(Resource.Id.NoteBody).Text = _Notes[position].Body.Substring(0, 50) + "...";
            }

            // Return the view.
            return view;
        }
    }
}