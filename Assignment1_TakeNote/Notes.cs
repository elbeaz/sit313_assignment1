using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

// SQLite Class.
using SQLite;

namespace Assignment1_TakeNote
{
    class Notes
    {
        // This class is used to store data for a note. Including an ID, creation date,
        // title and body. Might add a modified date in here later.
        // Using Public getters and setters for the attributes.

            // SQLite class attribute, identifies NoteId as the primary key, to auto increment.
        [PrimaryKey, AutoIncrement]
        public int NoteId { get; set; }

        public DateTime CreationDate { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }
    }
}