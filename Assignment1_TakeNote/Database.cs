using System;
using SQLite;
using System.IO;

namespace Assignment1_TakeNote
{
    class Database
    {
        // Class used to manage the connection to the database.
        // Just means I don't have to manage the database file location in multiple places.
        public SQLiteConnection Connect()
        {
            string dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "takenote.db3");
            return new SQLiteConnection(dbPath);
        }
    }
}